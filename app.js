const passport = require("passport"); //same below
require("dotenv/config");
const api = process.env.API_URL;
const express = require("express");
const app = express();
const errorHandler = require("./helpers/error-handler");

// const bodyParser = require("body-parser");

// //middleware
// app.use(express.bodyParser());
// app.use(bodyParser());

//replace for bodyParser deprecated
app.use(express.json()); //body json
app.use(
  express.urlencoded({
    //body by x-www-form-urlencoded
    extended: true,
  })
);
// app.use(express.multipart()); //app.use(bodyParser.json());

//LOG
const morgan = require("morgan"); //showing log

//connect DATABASE
const mongoose = require("mongoose"); // khai báo
mongoose
  .connect(process.env.CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: "shop_nodejs",
    useCreateIndex: true, //DeprecationWarning: collection.ensureIndex is deprecated. Use createIndexes instead.
    useFindAndModify: false, //https://stackoverflow.com/questions/52572852/deprecationwarning-collection-findandmodify-is-deprecated-use-findonea
  })
  .then(() => {
    console.log("Database Connection is ready...");
  })
  .catch((err) => {
    console.log(err);
  });

//init Database Product
//https://expressjs.com/en/resources/middleware/cors.html check de biết cach xài cors!
// var corsOptions = {
//   origin: 'http://example.com',
//   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
// }

// app.get('/products/:id', cors(corsOptions), function (req, res, next) {
//   res.json({msg: 'This is CORS-enabled for only example.com.'})
// })
var allowlist = ['http://example1.com', 'http://example2.com']
const cors = require("cors");
app.use("/public/uploads", express.static(__dirname + "/public/uploads")); // để cho phép truy cập folder data ko cần khai bao route
app.use(cors());
app.options("*", cors());

//========================================================================================
//MIDDLEWARE
//omitEmpty anti null empty
const omitEmpty = require("omit-empty");
const removeEmptyProperties = () => {
  return function (req, res, next) {
    req.body = omitEmpty(req.body);
    req.params = omitEmpty(req.params);
    req.query = omitEmpty(req.query);
    next();
  };
};
app.use(removeEmptyProperties());
app.use(morgan("dev")); // showing log POST /api/v1/product 200 24 - 3.468 ms
// app.use(passport.initialize());//You are not required to use passport.initialize() if you are not using sessions.

// ========================================================================================
// //Routes
const categoriesRoutes = require("./routes/categories.router");
const productsRoutes = require("./routes/products.router");
const usersRoutes = require("./routes/users.router");
const ordersRoutes = require("./routes/orders.router");

app.use(`${api}/categories`, categoriesRoutes);
app.use(`${api}/products`, productsRoutes);
app.use(`${api}/users`, usersRoutes);
app.use(`${api}/orders`, ordersRoutes);

//========================================================================================
// //Database
//need go mongodb.com add IPlist if run production!
//After change pass should wait bien mat thanh xanh!
//We are deploying your changes (current action: creating a plan)
//Database Connection is ready...

app.listen(3000, () => {
  console.log("🍏 Server is running http://localhost:3000");
});

//Test show text welcome when go /

//http://localhost:3000/

//handle all error to json!
const jsonErrorHandler = async (err, req, res, next) => {
  res.status(500).send({ error: err, success: false });
};
// Your handler
app.use(jsonErrorHandler);
app.use(errorHandler);
app.get("/", (req, res) => {
  // res.send({'success' : true})
  res.send("connect OK success /");
});

// })
