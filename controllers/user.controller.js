const { User } = require("../models/user");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
//========================================================================================
const findAll = (req, res) => {
  //:page in url
  let filter = {
    // price: { $gt: 10000 },
  };
  let page = req.params.page;
  let per = 10;
  // let query = {};
  //more about options https://www.npmjs.com/package/mongoose-paginate-v2
  //{Boolean} - If lean and leanWithId are true, adds id field with string representation of _id to every document
  let options = {
    // offset: helper.getOffset(page, per), //page = offset
    page: page, //page = offset, no need calculator offset
    limit: per,
    lean: true,
    leanWithId: true,
    select: "name price", //only show name, price, id
    sort: { price: 1 }, // sort A -> Z theo price tu nho den lon'
    customLabels: { docs: "users" }, //not set default is docs name
  };

  console.log((page - 1) * per);
  User.paginate(filter, options)
    // User.paginate()
    // .select("-passwordHash")
    // .populate("category") // OPTIONNAL show luôn toàn bộ data của khoá ngoại chứ ko chỉ show ID cat
    .then((list) => {
      res.send({ data: list, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
};

//========================================================================================
// const getUserById = (req, res) => {
//   //check validate ID
//   console.log("getUserById ..");
//   const id = req.query.id; // tìm ID trên URL
//   // const id = req.param.id; // tìm ID theo dạng /:id
//   if (!mongoose.isValidObjectId(id)) {
//     return res.status(400).json({
//       error: "Invalid User",
//       success: false,
//     });
//   }
//   //start find
//   User.findById(id)
//     .select("-passwordHash")
//     .then((itemFound) => {
//       console.log("itemFound", itemFound);
//       if (itemFound != null) {
//         res.status(200).json({
//           data: itemFound,
//           success: true,
//         });
//       } else {
//         res.status(500).json({
//           error: "Not found",
//           success: false,
//         });
//       }
//     })
//     .catch((err) => {
//       res.status(500).json({
//         error: err,
//         success: false,
//       });
//     });
// };

//========================================================================================
const getProfile = (req, res) => {
  console.log("✅ user id ", req.user.id);
  return res.status(200).json({
    data: req.user,
    success: true,
  });
};
//========================================================================================

const created = async (req, res) => {
  //check FK exits or not

  const newData = {
    name: req.body.name,
    email: req.body.email,
    passwordHash: bcrypt.hashSync(req.body.password, 10),
    phone: req.body.phone,
    isAdmin: req.body.isAdmin,
    street: req.body.street,
    apartment: req.body.apartment,
    zip: req.body.zip,
    city: req.body.city,
    country: req.body.country,
  };
  User(newData)
    .save()
    .then((createdItem) => {
      res.status(200).json({
        data: "Success",
        success: true,
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
};

//========================================================================================
const login = async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  const secret = process.env.SECRECT_TOKEN_STRING;
  if (!user) {
    return res.status(400).send("The user not found");
  }

  //#1 nếu có user và password khách gửi mả hoá sau đó trùng với server thì
  //#2 tạo 1 chuỗi token, chuỗi này có thể dịch ngược từ server và đọc các biến trong đó
  //cách này hay khi ko cần phải query ngược lại từ token để bết ai với ai. có thể bỏ vào expire time để quyết luôn!
  //#3 Chuỗi giãi mã bí mật!
  //#4 : Thời hạn của token!
  ///#1
  if (user && bcrypt.compareSync(req.body.password, user.passwordHash)) {
    const token = jwt.sign(
      //#2
      {
        userId: user.id,
        // isAdmin: user.isAdmin,//for test only
        // isPhung: true,
      },
      secret //#3
      // { expiresIn: "1d" } //#4 ko can hệt hạn thì tắt { expiresIn: '365d' }
    );
    res.status(200).json({
      data: { user: user.email, token: token },
      success: true,
    });
  } else {
    res.status(400).json({
      error: "password is wrong!",
      success: false,
    });
  }
};

//========================================================================================
module.exports = {
  findAll,
  // getUserById,
  created,
  login,
  getProfile,
};

//Export ở đây để bên ngoài gọi bằng
//Bên ngoài ở user.router.js
//const controller = require("../controllers/user.controller.js");
//Và bên ngoài sử dụng controller.findAll
