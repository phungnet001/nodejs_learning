//Xử lý việc gọi database và return
//nếu ko trong ngoặt tức là gọi luôn  model Category { Item: Model { Category } }
//ở đây ko thể dùng { Item vì xuất trong modul ra là Item trùng tên}
const { Category } = require("../models/category");
const { Product } = require("../models/product");
const mongoose = require("mongoose");
const helper = require("../helpers/helper");
//========================================================================================
//https://stackoverflow.com/questions/10811887/how-to-get-all-count-of-mongoose-model
//3 cách lấy.. dùng then la OK nhất!
const getCount = (req, res) => {
  console.log("getCount ..");
  //test static condition
  // let conditionFilter = {
  //   name: "haha",
  // }
  // let conditionFilter = req.body; // tìm theo json hoac body truyền vào..
  let conditionFilter = req.body; // tìm theo json hoac body truyền vào..
  Product.countDocuments(conditionFilter)
    .then((count) => {
      console.log("getCount .", count);
      res.send({ total: count, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
};

//tìm tất cả product có isFeatire = true, giớ hạn chỉ lấy 1 số lượng per
const getFeature = (req, res) => {
  console.log("getFeature ..");
  //test static condition
  let conditionFilter = {
    // tìm tất cả produc có isFeatured true
    isFeatured: true,
  };
  let per = req.params.per ? req.params.per : 0; //nếu có thì lấy ko có thì 0
  Product.find(conditionFilter)
    .limit(+per) // thêm + trước per để biến nó về 0
    // .populate("category") // OPTIONNAL show luôn toàn bộ data của khoá ngoại chứ ko chỉ show ID cat
    .then((list) => {
      res.send({ data: list, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
};
//========================================================================================
const findAll = (req, res) => {
  Product.find()
    //https://medium.com/swlh/mongodb-pagination-fast-consistent-ece2a97070f3
    //query lan 1 : 608f629a9eba1906504e748e
    //query lan 1 thi lấy dieu kien lon hon 608f629a9eba1906504e748e
    // .limit(2)
    // .populate("category") // OPTIONNAL show luôn toàn bộ data của khoá ngoại chứ ko chỉ show ID cat
    .then((list) => {
      res.send({ data: list, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
};
//========================================================================================
//https://medium.com/swlh/mongodb-pagination-fast-consistent-ece2a97070f3
//tham khao them ve performance!
//filter all price > 10k and sort a->z
const findAllSortPaging = (req, res) => {
  //:page in url
  let filter = {
    price: { $gt: 10000 },
  };
  let page = req.params.page;
  let per = 2;
  // let query = {};
  //more about options https://www.npmjs.com/package/mongoose-paginate-v2
  //{Boolean} - If lean and leanWithId are true, adds id field with string representation of _id to every document
  let options = {
    // offset: helper.getOffset(page, per), //page = offset
    page: page, //page = offset
    limit: per,
    lean: true,
    leanWithId: true,
    select: "name price", //only show name, price, id
    sort: { price: 1 }, // sort A -> Z theo price tu nho den lon'
    customLabels: { docs: "products" }, //not set default is docs name
  };

  console.log((page - 1) * per);
  Product.paginate(filter, options)
    // .sort({ price: 1 }) //price: 1 -> sort A->Z, -1 tu Z->A ga tu thap den cao, tu cao den thap.!
    // .populate("category") // OPTIONNAL show luôn toàn bộ data của khoá ngoại chứ ko chỉ show ID cat
    .then((list) => {
      res.send({ data: list, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
};

//========================================================================================
const findAllByCategory = (req, res) => {
  //http://localhost:3000/api/v1/products/categories?=111,222
  let filter = {};
  //input tu` query sẽ là mỗi cat cách nhau dấu phẩy categories?=111,222
  if (req.query.categories) {
    filter = { category: req.query.categories.split(",") }; //tách dấu , để add các phần từ vào array! { category : ['111','222'] }
  }
  Product.find(filter)
    .populate("category") // OPTIONNAL show luôn toàn bộ data của khoá ngoại chứ ko chỉ show ID cat
    .then((list) => {
      res.send({ data: list, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
};
//========================================================================================
const findAllByPrice = (req, res) => {
  //http://localhost:3000/api/v1/products/filterPrice body { price : {min: 0, max : 10000} }
  // let filter = {};
  // //input tu` query sẽ là mỗi cat cách nhau dấu phẩy categories?=111,222
  // if (req.query.price) {
  //   filter = { price: price }; //tách dấu , để add các phần từ vào array! { category : ['111','222'] }
  // }
  // let filter = { price: { $gte: 5000 } }; //is greater than or equal to

  //https://docs.mongodb.com/manual/reference/operator/query-comparison/

  //$eq =
  //$gt >
  //$gte >=
  //$in Matches any of the values specified in an array.
  //$lte <=
  //$nin Matches none of the values specified in an array.
  // created_at: {
  //       $gte: ISODate("2010-04-29T00:00:00.000Z"),
  //       $lt: ISODate("2010-05-01T00:00:00.000Z")
  //   }
  let filter2 = { price: { $gte: 1000 } }; // >= 10k
  let filter = { price: { $gte: 1000, $lte: 15000 } }; // 10k <= item <=20k
  Product.find(filter)
    .populate("category") // OPTIONNAL show luôn toàn bộ data của khoá ngoại chứ ko chỉ show ID cat
    .then((list) => {
      res.send({ data: list, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
};
//========================================================================================
const findAllShowNamePriceOnly = (req, res) => {
  Product.find()
    .populate("category") // OPTIONNAL show luôn toàn bộ data của khoá ngoại chứ ko chỉ show ID cat
    .select("name price") //show name, price and id only
    .then((list) => {
      res.send({ data: list, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
};
//========================================================================================
const created = async (req, res) => {
  try {
    console.log("created product", Category);
    const id_cat = req.body.category;
    //check validate ID
    if (!mongoose.isValidObjectId(id_cat)) {
      return res.status(400).json({
        error: "Invalid Category",
        success: false,
      });
    }
    const category = await Category.findById(id_cat);
    console.log("Category found ", category);
    if (category.success == false)
      return res.status(400).send("Invalid Category");

    const file = req.file;
    if (!file) return res.status(400).send("No image in the request");

    const fileName = file.filename;
    const basePath = `${req.protocol}://${req.get("host")}/public/uploads/`; //lấy url
    //"http://localhost:3000/public/upload/image-2323232"

    const newData = {
      name: req.body.name,
      description: req.body.description,
      richDescription: req.body.richDescription,
      image: `${basePath}${fileName}`, // "http://localhost:3000/public/upload/image-2323232"
      brand: req.body.brand,
      price: req.body.price,
      category: req.body.category,
      countInStock: req.body.countInStock,
      rating: req.body.rating,
      numReviews: req.body.numReviews,
      isFeatured: req.body.isFeatured,
    };
    Product(newData)
      .save()
      .then((createdItem) => {
        res.status(200).json({
          data: createdItem,
          success: true,
        });
      })
      .catch((err) => {
        res.status(500).json({
          error: err,
          success: false,
        });
      });
  } catch (error) {
    return res.status(500).json({ message: error.message, success: false });
  }
};
//========================================================================================
const getId = (req, res) => {
  //check validate ID
  console.log("getId ..");
  const id = req.query.id; // tìm ID trên URL
  // const id = req.param.id; // tìm ID theo dạng /:id
  if (!mongoose.isValidObjectId(id)) {
    return res.status(400).json({
      error: "Invalid Product",
      success: false,
    });
  }
  //start find
  Product.findById(id)
    .populate("category") // show luôn toàn bộ data của khoá ngoại chứ ko chỉ show ID cat
    .then((itemFound) => {
      console.log("itemFound", itemFound);
      if (itemFound != null) {
        res.status(200).json({
          data: itemFound,
          success: true,
        });
      } else {
        res.status(500).json({
          error: "Not found",
          success: false,
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
};
//========================================================================================

const updated = (req, res) => {
  const newData = {
    name: req.body.name,
    description: req.body.description,
    richDescription: req.body.richDescription,
    image: req.body.image,
    brand: req.body.brand,
    price: req.body.price,
    category: req.body.category,
    countInStock: req.body.countInStock,
    rating: req.body.rating,
    numReviews: req.body.numReviews,
    isFeatured: req.body.isFeatured,
  };
  //check validate productin ID
  const id = req.params.id;
  if (!mongoose.isValidObjectId(id)) {
    return res.status(400).json({
      error: "Invalid Product",
      success: false,
    });
  }

  const id_cat = req.body.category;
  //check validate ID Cat
  if (!mongoose.isValidObjectId(id_cat)) {
    return res.status(400).json({
      error: "Invalid Category",
      success: false,
    });
  }

  Product.findByIdAndUpdate(
    id,
    newData,
    { new: true } //for update return new data after updated
  )
    .then((itemUpdated) => {
      return res.status(200).json({ data: itemUpdated, success: true });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
};
//========================================================================================
const deleted = (req, res) => {
  //check validate ID
  const id = req.params.id;
  if (!mongoose.isValidObjectId(id)) {
    return res.status(400).json({
      error: "Invalid Product",
      success: false,
    });
  }
  Product.findByIdAndRemove(req.params.id)
    .then((product) => {
      if (product) {
        return res
          .status(200)
          .json({ success: true, message: "the product is deleted!" });
      } else {
        return res
          .status(404)
          .json({ success: false, message: "product not found!" });
      }
    })
    .catch((err) => {
      return res.status(500).json({ success: false, error: err });
    });
};

const galleryUpdate = async (req, res) => {
  try {
    if (!mongoose.isValidObjectId(req.params.id)) {
      return res
        .status(400)
        .json({ success: false, message: "Invalid Product Id" });
    }
    const files = req.files; //array fiels from express req
    let imagesPaths = [];
    const basePath = `${req.protocol}://${req.get("host")}/public/uploads/`;

    if (files) {
      //if not undefine! then loop
      files.map((file) => {
        imagesPaths.push(`${basePath}${file.filename}`); // với mỗi phần từ file thì lấy full path add vào array string
      });
    }

    const product = await Product.findByIdAndUpdate(
      req.params.id, //ID của production cần update là gì
      {
        images: imagesPaths, // chỉ update images, ko update gì khác
      },
      { new: true } // thay thế xong thì refresh lại dat mới nhất !
    );

    if (!product) return res.status(500).send("the gallery cannot be updated!");

    res.send(product);
  } catch (error) {
    return res.status(500).json({ success: false, error: err });
  }
};

//========================================================================================
module.exports = {
  findAll,
  findAllSortPaging,
  findAllShowNamePriceOnly,
  created,
  getId,
  deleted,
  updated,
  getCount,
  getFeature,
  findAllByCategory,
  findAllByPrice,
  galleryUpdate,
};
