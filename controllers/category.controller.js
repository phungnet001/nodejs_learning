//Xử lý việc gọi database và return
const { Category } = require("../models/category"); //Item is Category
const mongoose = require("mongoose");
const sentEmail = require("./email.controller");
const sentNoti = require("./notification.controller");
// =========== =========== =========== =========== =========== ===========
const findAll = (req, res) => {
  Category.find()
    .then((list) => {
      res.send({ data: list, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
  sentEmail();
  sentNoti();
  //after file sent email test
};
// =========== =========== =========== =========== =========== ===========
const created = (req, res) => {
  const item = new Category({
    name: req.body.name,
    icon: req.body.icon,
    color: req.body.color,
  });
  item
    .save()
    .then((createdItem) => {
      res.status(200).json({
        data: createdItem,
        success: true,
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
};
// =========== =========== =========== =========== =========== ===========
const deleted = (req, res) => {
  //ID cua Cat la OBjectID nên phia check đúng chuẩn mới ko lỗi, còn productionID thì ko cần!
  //check validate ID
  const id = req.params.id;
  if (!mongoose.isValidObjectId(id)) {
    return res.status(400).json({
      error: "Invalid Category",
      success: false,
    });
  }
  Category.findByIdAndRemove(id)
    .then((category) => {
      if (category) {
        return res
          .status(200)
          .json({ success: true, message: "The category is deleted!" });
      } else {
        return res
          .status(404)
          .json({ success: false, message: "Category not found!" });
      }
    })
    .catch((err) => {
      return res.status(500).json({ success: false, error: err });
    });
};
// =========== =========== =========== =========== =========== ===========
const getId = (req, res) => {
  //must check ID valid 1st!
  //ID cua Cat la OBjectID nên phia check đúng chuẩn mới ko lỗi, còn productionID thì ko cần!
  //check validate ID
  const id = req.params.id;
  if (!mongoose.isValidObjectId(id)) {
    return res.status(400).json({
      error: "Invalid Category",
      success: false,
    });
  }
  Category.findById(req.params.id)
    .then((itemFound) => {
      console.log("itemFound", itemFound);
      if (itemFound != null) {
        res.status(200).json({
          data: itemFound,
          success: true,
        });
      } else {
        res.status(500).json({
          error: "Not found",
          success: false,
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
};
// // =========== =========== =========== =========== =========== ===========
// const updated = async (req, res) => {
//   const itemUpdated = await Item.findByIdAndUpdate(req.params.id, {
//     name: req.body.name,
//     icon: req.body.icon,
//     color: req.body.color,
//   });
//   if (itemUpdated)
//     return res.status(200).json({ data: itemUpdated, success: true });
//   res.status(200).json({ data: itemUpdated, success: true });
// };

// // =========== =========== =========== =========== =========== ===========

// =========== =========== =========== =========== =========== ===========
const updated = (req, res) => {
  const newData = {
    name: req.body.name,
    icon: req.body.icon,
    color: req.body.color,
  };
  //check validate ID
  const id = req.params.id;
  if (!mongoose.isValidObjectId(id)) {
    return res.status(400).json({
      error: "Invalid Category",
      success: false,
    });
  }

  Category.findByIdAndUpdate(
    id,
    newData,
    { new: true } //for update return new data after updated
  )
    .then((itemUpdated) => {
      return res.status(200).json({ data: itemUpdated, success: true });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
};

// =========== =========== =========== =========== =========== ===========
//DÙng cách này để gọi funtion từ các file khác
module.exports = {
  findAll,
  created,
  deleted,
  getId,
  updated,
};
