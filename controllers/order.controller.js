const { Order } = require("../models/order");
const { OrderItem } = require("../models/order-item");
const mongoose = require("mongoose");

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

//========================================================================================
//Admin show tất cả orders của tất cả trên hệ thống ! , có paging, per, có filter .., có sort!
const findAll = (req, res) => {
  //:page in url
  let filter = {
    // totalPrice: { $gt: 130000 }, //chỉ lọc hoá đơn nào trên 130k
  };
  let page = req.params.page;
  let per = 10;
  //more about options https://www.npmjs.com/package/mongoose-paginate-v2
  //{Boolean} - If lean and leanWithId are true, adds id field with string representation of _id to every document
  let options = {
    // offset: helper.getOffset(page, per), //page = offset
    page: page, //page = offset, no need calculator offset
    limit: per,
    lean: true,
    leanWithId: true,
    // select: "status totalPrice", //only show name, price, id
    sort: { dateOrdered: -1 }, // sort A -> Z theo price tu nho den lon'
    customLabels: { docs: "orders", totalDocs: "total" }, //not set default is docs name
    populate: "user", //có thể dùng option để add các thuộc tính khi tìm/ mỡ rộng field user thay vì chỉ ID! cho admin dễ nhìn ! nếu cần
  };

  console.log((page - 1) * per);
  Order.paginate(filter, options)
    // User.paginate()
    // .select("-passwordHash")
    // .populate("category") // OPTIONNAL show luôn toàn bộ data của khoá ngoại chứ ko chỉ show ID cat
    .then((list) => {
      res.send({ data: list, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
};
// //========================================================================================

//Admin show tất cả orders của tất cả trên hệ thống ! , có paging, per, có filter .., có sort!
const findAllMyOrder = (req, res) => {
  //:page in url
  console.log("req.user", req.user);
  let filter = {
    // totalPrice: { $gt: 130000 }, //chỉ lọc hoá đơn nào trên 130k
    user: req.user._id,
  };
  let page = req.params.page;
  let per = 10;
  //more about options https://www.npmjs.com/package/mongoose-paginate-v2
  //{Boolean} - If lean and leanWithId are true, adds id field with string representation of _id to every document
  let options = {
    // offset: helper.getOffset(page, per), //page = offset
    page: page, //page = offset, no need calculator offset
    limit: per,
    lean: true,
    leanWithId: true,
    // select: "status totalPrice", //only show name, price, id
    // sort: { price: 1 }, // sort A -> Z theo price tu nho den lon'
    customLabels: { docs: "orders", totalDocs: "total" }, //not set default is docs name
  };

  console.log((page - 1) * per);
  Order.paginate(filter, options)
    // User.paginate()
    // .select("-passwordHash")
    // .populate("category") // OPTIONNAL show luôn toàn bộ data của khoá ngoại chứ ko chỉ show ID cat
    .then((list) => {
      res.send({ data: list, success: true });
    })
    .catch((error) => {
      res.status(500).json({ error: error, success: false });
    });
};
//========================================================================================

const created = async (req, res) => {
  console.log("WTF");
  try {
    // loop to get orderItemsIds, moi orderItem chua quantity, product, và auto sinh ra id là orderItemsId
    // check all vapid product_id 1st, if user try add product_id error -> crash
    const orderItemsIds = Promise.all(
      req.body.orderItems.map(async (orderItem) => {
        //need vaidate product_id
        const product_id = orderItem.product;
        if (!mongoose.isValidObjectId(product_id)) {
          return res.status(400).json({
            error: "Invalid product_id " + product_id,
            success: false,
          });
        }

        let newOrderItem = new OrderItem({
          quantity: orderItem.quantity,
          product: product_id, //productID
        });
        //create new OrderItem
        newOrderItem = await newOrderItem.save(); //save xong return cái id
        //newOrderItem lúc này là 1 Promise, để loại bỏ Promise ta cần hứng nó bằng await
        return newOrderItem._id;
      })
    );
    //kết thúc được orderItemsIds là 1 array orderItemsId
    //newOrderItem lúc này là 1 Promise, để loại bỏ Promise ta cần hứng nó bằng await
    const orderItemsIdsResolved = await orderItemsIds; //dung trực tiếp luôn

    //Mỗi item tìm được thì lấy tiền * với số lượng đem tất cả cộng lại cho ra totalPrices
    const totalPrices = await Promise.all(
      orderItemsIdsResolved.map(async (orderItemId) => {
        //sau khi tao OrderItem thì tìm lại nó để tính totalPrice từ các product-> truy sâu tìm giá
        const orderItem = await OrderItem.findById(orderItemId).populate(
          "product",
          "price"
        );
        const totalPrice = orderItem.product.price * orderItem.quantity;
        return totalPrice;
      })
    );

    const totalPrice = totalPrices.reduce((a, b) => a + b, 0);
    // console.log("totalPrice:", totalPrice);

    let userID = req.user.id;
    console.log("req.user.id", userID);
    //create new Order
    let order = new Order({
      orderItems: orderItemsIdsResolved,
      shippingAddress1: req.body.shippingAddress1,
      shippingAddress2: req.body.shippingAddress2,
      city: req.body.city,
      zip: req.body.zip,
      country: req.body.country,
      phone: req.body.phone,
      status: req.body.status,
      totalPrice: totalPrice,
      user: userID,
    });
    //write to database
    order
      .save()
      .then((list) => {
        res.send({ data: list, success: true });
      })
      .catch((error) => {
        res.status(500).json({ error: error, success: false });
      });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// //========================================================================================
module.exports = {
  findAll,
  findAllMyOrder,
  created,
};

//Export ở đây để bên ngoài gọi bằng
//Bên ngoài ở user.router.js
//const controller = require("../controllers/user.controller.js");
//Và bên ngoài sử dụng controller.findAll
