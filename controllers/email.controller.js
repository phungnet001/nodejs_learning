"use strict";
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

//EMAIL
//====================================================================================================
const sentEmail = async (req, res) => {
  const msg = {
    to: "ios@yopmail.com",
    from: "	phung.duy@seedin.tech", //must verify this email on sendgrid !
    subject: "Sending with SendGrid is Fun",
    text: "and easy to do anywhere, even with Node.js",
  };
  sgMail
    .send(msg)
    .then(() => {
      console.log("sent email OK");
    })
    .catch((error) => {
      console.log("error sent email OK");
      console.error(error.response.body);
    });
};
//====================================================================================================

module.exports = sentEmail;
