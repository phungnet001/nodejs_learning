const { Category } = require("../models/category");
const express = require("express");
const router = express.Router();
const controller = require("../controllers/category.controller");

router.get(`/`, controller.findAll);
router.get(`/:id`, controller.getId);
router.post(`/`, controller.created);
router.delete(`/:id`, controller.deleted);
router.put(`/:id`, controller.updated);

// //GET FROM LIST FROM DATABASE dùng then tốt hơn vì có thể bắt được error
// router.get('/', (req, res) => {
//     //return 1 json product
//     console.log('GET FROM LIST FROM DATABASE dùng then tốt hơn vì có thể bắt được error')
//     Category.find().then(list => {
//         res.send({data: list, success : true})
//     }).catch((error) => {
//         res.status(500).json({error : error, success : false})
//     })

// })

// //POST SIMPLE ITEM!
// router.post(`/`, (req, res) =>{
//     const item = new Category({
//         name: req.body.name,
//         icon: req.body.icon,
//         color: req.body.color
//     })
//     item.save().then((createdItem=> {
//         res.status(201).json(createdItem)
//     })).catch((err)=>{
//         res.status(500).json({
//             error: err,
//             success: false
//         })
//     })
// })

// // router.get(`/`, async (req, res) =>{
// //     const categoryList = await Category.find();

// //     if(!categoryList) {
// //         res.status(500).json({success: false})
// //     }
// //     res.status(200).send(categoryList);
// // })

// router.get('/:id', async(req,res)=>{
//     const category = await Category.findById(req.params.id);

//     if(!category) {
//         res.status(500).json({message: 'The category with the given ID was not found.'})
//     }
//     res.status(200).send(category);
// })

// // router.post('/', async (req,res)=>{
// //     let category = new Category({
// //         name: req.body.name,
// //         icon: req.body.icon,
// //         color: req.body.color
// //     })
// //     category = await category.save();

// //     if(!category)
// //     return res.status(400).send('the category cannot be created!')

// //     res.send(category);
// // })

// router.put('/:id',async (req, res)=> {
//     const category = await Category.findByIdAndUpdate(
//         req.params.id,
//         {
//             name: req.body.name,
//             icon: req.body.icon || (category.icon),
//             color: req.body.color,
//         },
//         { new: true}
//     )

//     if(!category)
//     return res.status(400).send('the category cannot be created!')

//     res.send(category);
// })

// router.delete('/:id', (req, res)=>{
//     Category.findByIdAndRemove(req.params.id).then(category =>{
//         if(category) {
//             return res.status(200).json({success: true, message: 'the category is deleted!'})
//         } else {
//             return res.status(404).json({success: false , message: "category not found!"})
//         }
//     }).catch(err=>{
//        return res.status(500).json({success: false, error: err})
//     })
// })

module.exports = router;
