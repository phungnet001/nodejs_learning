const { Product } = require("../models/product");
const express = require("express");
const { Category } = require("../models/category");
const router = express.Router();
const mongoose = require("mongoose");
const multer = require("multer");
const controller = require("../controllers/product.controller.js");
const FILE_TYPE_MAP = {
  "image/png": "png",
  "image/jpeg": "jpeg",
  "image/jpg": "jpg",
};

const storage = multer.diskStorage({
  destination: function (req, file, cb, res) {
    const isValid = FILE_TYPE_MAP[file.mimetype];
    let uploadError = new Error("invalid image type");

    // try {
    //   if (isValid != undefined) {
    //     console.log("isValid ", isValid);
    //     uploadError = null;
    //     cb(uploadError, "public/uploads");
    //   } else {
    //     console.log("isValid == undefined", isValid);
    //     next();
    //   }
    // } catch (error) {
    //   console.log("error roi`");

    // }
    if (isValid != undefined) {
      uploadError = null;
    }
    cb(uploadError, "public/uploads");
  },
  filename: function (req, file, cb) {
    const fileName = file.originalname.split(" ").join("-");
    const extension = FILE_TYPE_MAP[file.mimetype];
    cb(null, `${fileName}-${Date.now()}.${extension}`);
  },
});

const uploadOptions = multer({ storage: storage });
// Lấy tất cả list
router.get(`/`, controller.findAll);
router.get(`/findAllSortPaging/:page`, controller.findAllSortPaging);
router.get(`/findAllShowNamePriceOnly`, controller.findAllShowNamePriceOnly);
router.get(`/findAllByCategory`, controller.findAllByCategory);
router.get(`/find`, controller.getId);
router.get(`/getCount`, controller.getCount);
router.get(`/getFeature/:per`, controller.getFeature);
router.get(`/findAllByPrice`, controller.findAllByPrice);

router.post(`/`, uploadOptions.single("image"), controller.created);
//Delete
router.delete(`/:id`, controller.deleted);
router.put(`/:id`, controller.updated);
router.put(
  `/galleryUpdate/:id`,
  uploadOptions.array("images", 10),
  controller.galleryUpdate
);

//http://localhost:3000/public/uploads/ic_filter.png-1621141120830.png ERROR vì ko được khai báo trong routee nào hết!

//galleryUpdate

//GET FROM LIST FROM DATABASE dùng then tốt hơn vì có thể bắt được error
//move vao controller !
// router.get('/', (req, res) => {
//     //return 1 json product
//     console.log('GET FROM LIST FROM DATABASE dùng then tốt hơn vì có thể bắt được error')
//     Product.find().then(list => {
//         res.send({data: list, success : true})
//     }).catch((error) => {
//         res.status(500).json({error : error, success : false})
//     })

// })

// //POST SIMPLE ITEM!
// router.post(`/`, (req, res) =>{
//     const item = new Product({
//         name: req.body.name,
//     })
//     item.save().then((createdItem=> {
//         res.status(201).json(createdItem)
//     })).catch((err)=>{
//         res.status(500).json({
//             error: err,
//             success: false
//         })
//     })
// })

// router.get(`/`, async (req, res) => {
//   let filter = {};
//   if (req.query.categories) {
//     filter = { category: req.query.categories.split(",") };
//   }

//   const productList = await Product.find(filter).populate("category");

//   if (!productList) {
//     res.status(500).json({ success: false });
//   }
//   res.send(productList);
// });

// router.get(`/:id`, async (req, res) =>{
//     const product = await Product.findById(req.params.id).populate('category');

//     if(!product) {
//         res.status(500).json({success: false})
//     }
//     res.send(product);
// })

// router.post(`/`, uploadOptions.single("image"), async (req, res) => {
//   console.log("created product", Category);
//   const id_cat = req.body.category;
//   //check validate ID
//   if (!mongoose.isValidObjectId(id_cat)) {
//     return res.status(400).json({
//       error: "Invalid Category",
//       success: false,
//     });
//   }
//   const category = await Category.findById(id_cat);
//   console.log("Category found ", category);
//   if (category.success == false)
//     return res.status(400).send("Invalid Category");

//   const file = req.file;
//   if (!file) return res.status(400).send("No image in the request");

//   const fileName = file.filename;
//   const basePath = `${req.protocol}://${req.get("host")}/public/uploads/`; //lấy url
//   //"http://localhost:3000/public/upload/image-2323232"

//   const newData = {
//     name: req.body.name,
//     description: req.body.description,
//     richDescription: req.body.richDescription,
//     image: `${basePath}${fileName}`, // "http://localhost:3000/public/upload/image-2323232"
//     brand: req.body.brand,
//     price: req.body.price,
//     category: req.body.category,
//     countInStock: req.body.countInStock,
//     rating: req.body.rating,
//     numReviews: req.body.numReviews,
//     isFeatured: req.body.isFeatured,
//   };
//   Product(newData)
//     .save()
//     .then((createdItem) => {
//       res.status(200).json({
//         data: createdItem,
//         success: true,
//       });
//     })
//     .catch((err) => {
//       res.status(500).json({
//         error: err,
//         success: false,
//       });
//     });
// });

// router.put('/:id',async (req, res)=> {
//     if(!mongoose.isValidObjectId(req.params.id)) {
//        return res.status(400).send('Invalid Product Id')
//     }
//     const category = await Category.findById(req.body.category);
//     if(!category) return res.status(400).send('Invalid Category')

//     const product = await Product.findByIdAndUpdate(
//         req.params.id,
//         {
//             name: req.body.name,
//             description: req.body.description,
//             richDescription: req.body.richDescription,
//             image: req.body.image,
//             brand: req.body.brand,
//             price: req.body.price,
//             category: req.body.category,
//             countInStock: req.body.countInStock,
//             rating: req.body.rating,
//             numReviews: req.body.numReviews,
//             isFeatured: req.body.isFeatured,
//         },
//         { new: true}
//     )

//     if(!product)
//     return res.status(500).send('the product cannot be updated!')

//     res.send(product);
// })

// router.delete('/:id', (req, res)=>{
//     Product.findByIdAndRemove(req.params.id).then(product =>{
//         if(product) {
//             return res.status(200).json({success: true, message: 'the product is deleted!'})
//         } else {
//             return res.status(404).json({success: false , message: "product not found!"})
//         }
//     }).catch(err=>{
//        return res.status(500).json({success: false, error: err})
//     })
// })

// router.get(`/get/count`, async (req, res) =>{
//     const productCount = await Product.countDocuments((count) => count)

//     if(!productCount) {
//         res.status(500).json({success: false})
//     }
//     res.send({
//         productCount: productCount
//     });
// })

// router.get(`/get/featured/:count`, async (req, res) =>{
//     const count = req.params.count ? req.params.count : 0
//     const products = await Product.find({isFeatured: true}).limit(+count);

//     if(!products) {
//         res.status(500).json({success: false})
//     }
//     res.send(products);
// })

// router.put(
//     '/gallery-images/:id',
//     uploadOptions.array('images', 10),
//     async (req, res)=> {
//         if(!mongoose.isValidObjectId(req.params.id)) {
//             return res.status(400).send('Invalid Product Id')
//          }
//          const files = req.files
//          let imagesPaths = [];
//          const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`;

//          if(files) {
//             files.map(file =>{
//                 imagesPaths.push(`${basePath}${file.filename}`);
//             })
//          }

//          const product = await Product.findByIdAndUpdate(
//             req.params.id,
//             {
//                 images: imagesPaths
//             },
//             { new: true}
//         )

//         if(!product)
//             return res.status(500).send('the gallery cannot be updated!')

//         res.send(product);
//     }
// )

module.exports = router;
