function getOffset(currentPage = 1, listPerPage) {
  return (currentPage - 1) * [listPerPage];
  // lay gia tru tu dong thu may, vi du page 1, tuc la ko co dich g`i het.. return 0 vi 1-1 * any = 0
  // page 2, thi` lay 2-1 * 10, tuc la offset 10... tuc1 la bo qua 10 con dau tien...tra ve` 10 con tiep theo
  //great helper!
}

function emptyOrRows(rows) {
  if (!rows) {
    return []; //anti null!
  }
  return rows;
}
function isEmpty(val) {
  let typeOfVal = typeof val;
  switch (typeOfVal) {
    case "object":
      return val.length == 0 || !Object.keys(val).length;
      break;
    case "string":
      let str = val.trim();
      return str == "" || str == undefined;
      break;
    case "number":
      return val == "";
      break;
    default:
      return val == "" || val == undefined;
  }
}

module.exports = {
  getOffset,
  emptyOrRows,
  isEmpty,
};
