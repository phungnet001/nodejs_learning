const { User } = require("../models/user");
const passport = require("passport");
var JwtStrategy = require("passport-jwt").Strategy,
  ExtractJwt = require("passport-jwt").ExtractJwt;
let opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.SECRECT_TOKEN_STRING;
console.log("try ccc");
// passport.js
passport.use(
  new JwtStrategy(opts, (token, done) => {
    return done(null, token);
  })
);
//https://github.com/mikenicholson/passport-jwt/issues/153
/**
 * middleware for checking authorization with jwt
 */
function authorized(request, response, next) {
  passport.authenticate("jwt", { session: false }, async (error, token) => {
    if (error || !token) {
      return response
        .status(401)
        .json({ message: "Token expire, try login again" });
    }
    try {
      User.findOne({ _id: token.userId }, function (err, user) {
        if (err) {
          console.log("🛑err found", err);
          response.status(401).json({ message: "Something went wrong" });
          next();
        }
        if (user) {
          console.log("✅ user found :", user);
          request.user = user;
          next();
        } else {
          console.log("🛑 not found ");
          response.status(401).json({ message: "Not found user" });
          next();
          // or you could create a new account
        }
      });
      // .select("name email isAdmin"); //only get some info!// need viet 1bham moi de xuat info get prfile
    } catch (error) {
      next(error);
    }
  })(request, response, next);
}
module.exports = authorized;
