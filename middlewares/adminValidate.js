// const ADMIN_TYPE = "isAdmin";// onlyb 1 type nen ko cần
const isAdmin = (req, res, next) => {
  console.log("checking isAdmin", req.user);
  // console.log("user", req);
  const { user } = req || {};
  const { isAdmin } = user;
  console.log("user.isAdmin", user);
  if (user.isAdmin != true) {
    return res
      .status(401)
      .json({ message: "The user is not admin", success: false });
  }
  next();
};
module.exports = isAdmin;
