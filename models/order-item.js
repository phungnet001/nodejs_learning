const mongoose = require("mongoose");

const orderItemSchema = mongoose.Schema({
  quantity: {
    type: Number,
    required: true,
  },
  product: {
    type: mongoose.Schema.Types.ObjectId, //FK to ProductID here
    ref: "Product",
  },
});

exports.OrderItem = mongoose.model("OrderItem", orderItemSchema);

//orderItem ko cần id, FK to Product
