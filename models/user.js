const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const uniqueValidator = require("mongoose-unique-validator");
const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  passwordHash: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  street: {
    type: String,
    default: "",
  },
  apartment: {
    type: String,
    default: "",
  },
  zip: {
    type: String,
    default: "",
  },
  city: {
    type: String,
    default: "",
  },
  country: {
    type: String,
    default: "",
  },
});

// productSchema.virtual("id").get(function () {
//   // tạo thêm 1 biến id lấy dta từ _id
//   return this._id.toHexString();
// });

userSchema.set("toJSON", {
  virtuals: true,
  versionKey: false, // no more _v
  transform(doc, ret) {
    //no more _id
    ret.id = ret._id;
    // delete ret._id;// keep for index
  },
});
userSchema.plugin(uniqueValidator); //for unique fiels as email
userSchema.plugin(mongoosePaginate); //for paging!

exports.userSchema = userSchema;

exports.User = mongoose.model("User", userSchema);
