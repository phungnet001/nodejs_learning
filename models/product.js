const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const productSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  richDescription: {
    type: String,
    default: "",
  },
  image: {
    type: String,
    default: "",
  },
  images: [
    {
      type: String,
    },
  ],
  brand: {
    type: String,
    default: "",
  },
  price: {
    type: Number,
    default: 0,
  },
  category: {
    // tham chieu' khoá ngoái là ID
    type: mongoose.Schema.Types.ObjectId, // //FKkey loại data là ID
    ref: "Category", // khoái ngoại tham chiếu đến bảng Category
    required: true, // bắt buộc 1 sản phẩm phải thuộc về 1 Cat nào đó
  },
  countInStock: {
    type: Number,
    required: true,
    min: 0, //set min max cho 1 field !
    max: 255,
  },
  rating: {
    type: Number,
    default: 0,
  },
  numReviews: {
    type: Number,
    default: 0,
  },
  isFeatured: {
    type: Boolean,
    default: false,
  },
  dateCreated: {
    type: Date,
    default: Date.now,
  },
});

// productSchema.virtual("id").get(function () {
//   // tạo thêm 1 biến id lấy dta từ _id
//   return this._id.toHexString();
// });

productSchema.set("toJSON", {
  virtuals: true,
  versionKey: false, // no more _v
  transform(doc, ret) {
    //no more _id
    ret.id = ret._id;
    // delete ret._id;// keep for index
  },
});
productSchema.plugin(mongoosePaginate); //for paging!
exports.Product = mongoose.model("Product", productSchema);
