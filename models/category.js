const mongoose = require("mongoose");

const categorySchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  icon: {
    type: String,
    required: true,
  },
  color: {
    type: String,
    required: true,
  },
});

// categorySchema.virtual("id").get(function () {
//   // tạo thêm 1 biến id lấy dta từ _id
//   return this._id.toHexString();
// });

//trả về JSON
categorySchema.set("toJSON", {
  virtuals: true,
  versionKey: false, // no more _v
  transform(doc, ret) {
    //no more _id
    ret.id = ret._id;
    // delete ret._id;// keep for index
  },
});

exports.Category = mongoose.model("Category", categorySchema); //Item is Category
